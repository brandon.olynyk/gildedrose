﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using csharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.Tests
{
    [TestClass()]
    public class GildedRoseTests
    {
        [TestMethod()]
        public void DegradationTest()
        {
            //After each day, the system lowers both sellin and quality values for each item.
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual("foo", Items[0].Name);
        }
        [TestMethod()]
        public void foo()
        {
            //Example test case
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual("foo", Items[0].Name);
        }
        [TestMethod()]
        public void DoubleDegradationTest()
        {
            //Once the sell date has passed, quality degrades twice as fast
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual("foo", Items[0].Name);
        }
        [TestMethod()]
        public void QualityNeverNegativeTest()
        {
            //Quality is never negative
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = -7979 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(0, Items[0].Quality);
        }
        [TestMethod()]
        public void AgedBrieIncreaseTest()
        {
            //Aged Brie increases in quality the older it gets
            IList<Item> Items = new List<Item> { new Item { Name = "Aged Brie", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.IsTrue(Items[0].Quality > 0);
        }
        [TestMethod()]
        public void Quality50CapTest()
        {
            //Quality is capped at 50
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = 1, Quality = 252525 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(50, Items[0].Quality);
        }
        [TestMethod()]
        public void SulfurasTest()
        {
            //Sulfuras, a legendary item never has to be sold or decreases in quality
            IList<Item> Items = new List<Item> { new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 2, Quality = 23 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            //Sellin doesn't decrease
            Assert.AreEqual(1, Items[0].SellIn);
            //Quality is forced to 80
            Assert.AreEqual(80, Items[0].Quality);
            
        }
        [TestMethod()]
        public void BackstagePassTest()
        {
            //Backstage passes increases in quality as the sellin value approaches.
            //Quality drops to 0 after the concert
            //Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less
            IList<Item> Items = new List<Item> { new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 10, Quality = 0 }, 
                new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 2, Quality = 0 },
            new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = -2, Quality = 24 }};
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            //10 days & less? Quality += 2
            Assert.AreEqual(2, Items[0].Quality);
            //5 days & less? Quality += 3
            Assert.AreEqual(3, Items[1].Quality);
            //Past sell date? Quality = 0
            Assert.AreEqual(0, Items[2].Quality);
        }
        [TestMethod()]
        public void ConjuredTest()
        {
            //Conjured items degrade 2x as fast as normal items
            IList<Item> Items = new List<Item> { new Item { Name = "Conjured Mana Cake", SellIn = 3, Quality = 45 }, new Item { Name = "Conjured Mana Cake", SellIn = -3, Quality = 45 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            //should degrade 2x as fast (-2). Might have "conjured brie, conjured backstage passes etc?"
            Assert.AreEqual(43, Items[0].Quality);
            //If below sell date, make that negation x2 (2*2=4)
            Assert.AreEqual(41, Items[1].Quality);
        }
    }
}